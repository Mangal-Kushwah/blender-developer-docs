# Blender 2.92: Modeling

## Tools

- Primitive Add: new interactive tool to create primitives with two
  clicks.
  (blender/blender@122cb1a)

![Add object tool](../../images/Add_object_with_objects_in_background.png){style="width:700px;"}

<figure>
<video src="../../../videos/Add_object.mp4" title="Add object tool" width="700" controls=""></video>
<figcaption>Add object tool</figcaption>
</figure>

## Modifiers

- Operator to copy a single modifier to all selected objects
  (blender/blender@6fbeb6e2e05408af448e)
- The bevel modifier now uses the angle limit method by default
  (blender/blender@6b5e4ad5899d87a183c4a3)
