# Blender 2.92: Asset Browser

Blender 2.92 includes a new editor: Asset Browser.

This is the first part of the Asset Browser project: basic listing and
browsing for [Blender
data-blocks](https://docs.blender.org/manual/en/latest/files/data_blocks.html)
saved locally.

![](../../images/Release_Notes-_Asset_Browser.png){style="width:640px;"}

The [About Milestone 1](#about-milestone-1) section gives
more information about what to expect from the current implementation.

  

## Workflow

### Creating Assets

![](../../images/Release_Notes-_Material,_Mark_Asset.png)

The *Mark Asset* operator turns a data-block into an asset data-block.
It's available in a few places:

- Right-click a data-block in the Outliner \> *Mark Asset* or Outliner
  \> *ID Data* \> *Mark Asset*. This way you can also select multiple
  data-blocks and mark them as assets.
- Right-click a button that represents a data-block (e.g. a [data-block
  selector](https://docs.blender.org/manual/en/latest/interface/controls/templates/data_block.html)
  or material slot) \> *Mark Asset*.
- Search menu in the Outliner.
- **Save the file**, if you want the new asset data-block to be
  available in other projects.

There's also the *Clear Asset* operator. It removes all asset metadata
and turns an asset data-block back into a regular data-block.

  

### Mounting Asset Libraries

![](../../images/Release_Notes-_Asset_Library_Preferences.png)

It is possible to select any directory as asset library. This can be
done in Preferences \> File Paths \> Asset Libraries.  
There is a default library pointing to the following paths:

| Platform | Path                                              |
|----------|---------------------------------------------------|
| Windows  | `C:\Users\\*username*\Documents\Blender\Assets\\ |
| Linux    | `~/Documents/Blender/Assets/`                   |
| MacOS    | `~/Documents/Blender/Assets/`                   |

  

### Using Assets

![](../../images/Release_Notes-_Asset_Browser_Dragging.png){style="width:450px;"}

The Asset Browser is a new, regular editor.

In the Asset Browser, you can:

- Select an asset library to browse in.
- Choose a category of assets to show.
- Drag collection, object, image and material assets into 3D Views.
- View metadata of the active asset.
- Edit metadata of the active asset in the *Current File* asset library.

*Most display and filter options won't work yet, that is expected.*

![](../../images/Release_Notes-_Asset_library_selector.png)

  

## Additional Features

- Support for generating object previews.
  (blender/blender@4b0396695c62)
- Support for loading custom images as data-block previews.
  (blender/blender@812ea9184221)
- Operator to regenerate automatic data-block previews.
  (blender/blender@58d818f8bebf)
- Asset data-blocks show a asset icon in the Outliner and search menus.
  (blender/blender@3fc9fc1cb46a)
- Various Python API additions, check the [dedicated release
  notes](python_api.md).
- *Only Assets* options for link and append.

## About Milestone 1

For the first milestone, the user takes the role of the asset manager
and is in charge over where and how assets are stored. The Asset Browser
gives a view into user-managed asset libraries.

**Use-Cases**  
The main use-case is local user asset libraries. That is, a user can
store data-blocks for own reuse in self defined locations on the hard
drive, and conveniently access them from any project.  
For simple projects that may also be enough, where all .blends are in a
single directory or in few directories (which can individually be
mounted as asset libraries in the Preferences).

**Notes**  
\* Data-blocks are not automatically assets. They have to be explicitly
be marked as such.

- An asset library is a directory containing .blend files. Each of the
  .blend files can contain asset data-blocks.
- To make an asset library available in other projects, it has to be
  mounted in the Preferences.
- Asset creation and metadata editing can only be done for the current
  file.
  - I.e. you can't just drag a data-block into an asset library, or move
    assets between asset libraries from within Blender.
  - So to edit an asset library, you have to open one of its .blend
    files and do the edits there.
  - This way, Blender doesn't automatically have to decide how the asset
    will be stored inside the asset library and how to deal with
    dependencies (e.g. the image texture of a material). The user is
    fully in charge of managing that.
  - **Save the file** to make the edits to the asset library available
    elsewhere.
- The *Current File* asset library is special:  
  It always shows the assets of the current file (no manual refreshing
  or saving needed) and it is the only asset library that allows editing
  of asset metadata (e.g. name, preview, description or tags). So like
  for asset creation, to edit the metadata of an asset, the file that
  contains the asset has to be opened, edited (via the *Current File*
  asset library) and saved.
- For when an Asset Browser shows an external asset library that was
  edited, there is a refresh button next to the asset library selector.

**More Information**  
For more high-level design information, refer to [this design
overview](https://developer.blender.org/project/view/124/) or [this
blogpost](https://code.blender.org/2020/03/asset-manager/).

(In old documents about the Asset Browser/Manager project, the term
"repository" may be used for libraries still.)
