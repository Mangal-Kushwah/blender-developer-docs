# Blender 4.2: EEVEE & Viewport

## Viewport Compositor

- The compositing space is now always limited to the camera region in camera view regardless of the
  passepartout value. Which means areas outside of the camera will not be composited, but the result
  will match the final render better.
