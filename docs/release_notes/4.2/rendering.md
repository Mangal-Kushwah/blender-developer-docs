# Blender 4.2: Rendering

- Motion Blur settings are now shared between Cycles and EEVEE. (blender/blender@74b8f99b43)
