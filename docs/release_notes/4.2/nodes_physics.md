# Blender 4.2: Nodes & Physics

### Geometry Nodes
* The *Sample Nearest Surface* node now supports group ids. (blender/blender@75e9056cac24d319bd5bb65dd13115ddad9074a9)
* The *Geometry Proximity* node now supports group ids. (blender/blender@0494605e7e422ad0980b35e4cc5f3703c02982ea)
* The sockets in the *Repeat* and *Simulation* zone nodes are now aligned. (blender/blender@1f30f41af885aeb50c1d173a0303921a09dac421)
* The *Remove Named Attribute* node now supports removing multiple attributes that have the same prefix or suffix. (blender/blender@6c46178a7fe342ffc8a3c92a7d1cd42ee9483c21)

## Node Editor
* Input sockets that don't allow editing the value don't show up in the sidebar anymore. (blender/blender@d27a1c47fafb84ebf0649423102a9e79b9a201b7, blender/blender@d9d9ff1dcd2036997064f982bbb94cf7d52e4828)