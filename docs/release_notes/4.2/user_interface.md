# Blender 4.2: User Interface

## General

- Support for dropping strings & URL's into the text editor & Python console
  (blender/blender@64843cb12d543f22be636fd69e9512f995c93510)


### Unused IDs Purge UI Improvements

The purge operation now pops-up a dialog where user can choose which options to apply, and get instant feedback on the amounts of unused data-blocks will be deleted.

![New Purge UI popup](ui_new_purge_popup.png)

blender/blender!117304, blender/blender@0fd8f29e88
