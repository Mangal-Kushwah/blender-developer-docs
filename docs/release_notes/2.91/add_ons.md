# Blender 2.91: Add-ons

# Add-ons Updates

## BlenderKit

![](../../images/Hair_material_preview_-_blenderkit.png)

- 2 new material previews were introduced:
  - Complex Ball. This preview is intended to show off specific material
    properties - edge wear or cavity detail, translucency and SSS,
    refraction.
  - Hair preview.
- Selected model panel was improved, now all commands work as expected.

## Collection Manager

<table markdown>
<tbody markdown>
<tr markdown>
<td markdown>
![Collection Manager popup with new Holdout and Indirect Only RTOs](../../images/Collection_Manager_2.91_Release_Notes.png){style="width:429px;"}
<figcaption>Collection Manager popup with new Holdout and Indirect Only
RTOs</figcaption>
</td>
<td markdown>
![QCD widgets with the new Quick View Toggles button to the left of the header widget. ](../../images/QCD_Screenshot_2.91.png){style="width:429px;"}
<figcaption>QCD widgets with the new Quick View Toggles button to the
left of the header widget. </figcaption>
</td>
</tr>
</tbody>
</table>

### New Features

#### CM Popup

- Added support for the Holdout and Indirect Only RTOs.
  (blender/blender-addons@559fbf90)
- Include new collections in the current filter until the filtering
  changes.
  (blender/blender-addons@52fb8e51)
- Allow all filters to be combined with each other.
  (blender/blender-addons@b66e1363)

#### QCD

![Quick View Toggles](../../images/Quick_View_Toggles_Blender_2.91.png)

- Added the ability to select all objects in a QCD slot when
  alt-clicking on the slot.
  (blender/blender-addons@1d1bb1a7)
- Added Quick View Toggles for influencing QCD setup/visibility.
  (blender/blender-addons@bf176041)
  - Enable All QCD Slots.
  - Enable All QCD Slots Isolated.
  - Disable All Non QCD Slots.
  - Disable All Collections.
  - Select All QCD Objects.
  - Discard QCD History.

### Bug Fixes

#### CM Popup

- Fixed the active object sometimes getting lost when performing actions
  with the exclude checkbox.
  (blender/blender-addons@975f81d2)
- Prevent new collections from being added when the selected collection
  isn't visible.
  (blender/blender-addons@52fb8e51)

#### QCD

- Fixed bugs with QCD slot switching.
  (blender/blender-addons@bf176041)
- Fixed the active object sometimes getting lost when toggling QCD
  slots.
  (blender/blender-addons@bf176041)
- Fixed the layout and display of theme overrides for the OpenGL move
  widget in the preferences.
  (blender/blender-addons@b8159369)
