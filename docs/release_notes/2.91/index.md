# Blender 2.91 Release Notes

Blender 2.91 was released on November 25, 2020.

Check out the final [release notes on
blender.org](https://www.blender.org/download/releases/2-91/).

## [User Interface](user_interface.md)

## [Modeling](modeling.md)

## [Sculpt](sculpt.md)

## [Grease Pencil](grease_pencil.md)

## [Volume Object](volumes.md)

## [EEVEE](eevee.md)

## [I/O & Overrides](io.md)

## [Python API](python_api.md)

## [Physics](physics.md)

## [Animation & Rigging](animation_rigging.md)

## [Add-ons](add_ons.md)

## [More Features](more_features.md)
