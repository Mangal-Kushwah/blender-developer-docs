# Blender 2.91: EEVEE

## Multi-Scatter GGX

The principle BSDF and Glossy BSDF nodes have been improved to support a
multi-scattering approximation, removing the energy loss on rough
surfaces.

![Rough metals with GGX (top) and GGX with multi-scattering approximation (bottom) ](../../images/Eevee_ggx_multiscatter.png){style="width:750px;"}

## Motion Blur

Shutter start position can now be adjusted to make motion trail effects
just like Cycles.
blender/blender@b7afcdff7b06

## Bug Fixes

- Screen Space Reflection now are more robust with less self
  intersections for rough reflections and less gaps at intersecting
  geometries.
  blender/blender@b55c44e3eda0
  blender/blender@27137c479cc3
- Fixed Camera Motion Blur is not blending steps properly
  blender/blender@aae60f0fecf.
- Fixed light probe baking ignoring indirect bounces from SSS
  blender/blender@568dc2665e8.
- Fixed Alpha Clip shadows actually using Alpha Hashed shadows
  blender/blender@96e8dadda0b.
- Fixed Environmental light leak with baked indirect lighting and
  refraction
  blender/blender@dede4aac5be9.
