# Blender 4.1: Add-ons

## Rigify

- Nested bone collections are now supported by the Rig Layers UI: when a parent
  collection is hidden, buttons for its children are greyed out.
  (blender/blender-addons@69f9e45f7b439)
- Some custom properties now use recently added boolean (checkbox/toggle) and enum (dropdown) types. (blender/blender-addons@0585a98f16c039cd6, [Manual](https://docs.blender.org/manual/en/4.1/addons/rigging/rigify/rig_features.html#limbs))
