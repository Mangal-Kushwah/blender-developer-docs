# Blender 4.1: Cycles

## OpenImageDenoise GPU Acceleration

OpenImageDenoise is now GPU accelerated on supported hardware. This makes full quality denoising available at interactive rates in the 3D viewport. (blender/blender#115045)

It is enabled automatically when using GPU rendering in the 3D viewport and for final renders. It can be disabled in the denoising settings panel, to reduce GPU memory usage at the cost of slower denoising.

### Supported GPUs

* NVIDIA GTX 16xx, TITAN V and all RTX GPUs
* AMD RDNA2 or RDNA3 generation discrete GPUs
* Intel GPUs with Xe-HPG architecture or newer
* Apple Silicon with macOS version 13.0 and newer

## Other

- Option to disable bump map correction
  (blender/blender@36e603c430ca90a4a19574a5071db54a7cebcb39)
- AMD GPU rendering support added for RDNA3 generation APUs
  (blender/blender@d19ad12b45c14dee9d17272cbc1d1b22d3a725aa)
- Linux CPU rendering performance was improved by about 5% across benchmarks 
  (blender/blender!116663)