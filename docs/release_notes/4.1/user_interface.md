# Blender 4.1: User Interface

## General

- Input Placeholders to show a hint about the expected value of an
  input.
  (blender/blender@b0515e34f9).

![](../../images/Placeholders.png){style="width:400px;"}

- Cryptomatte picking can now occur between separate windows.
  (blender/blender@9ee5de05c0).
- The UI interface font can now be shown in any weight.
  (blender/blender@0bde01eab5).

![](../../images/UIFontweight.png){style="width:400px;"}

- Khmer font added to support a new translation of that Cambodian
  language.
  (blender/blender@3f5654b491).
- New icons added to represent area splitting, joining, and swapping.
  (blender/blender@153dd76d22,
  blender/blender@8933284518).
- Wide Enum lists will now collapse to a single column if not enough
  space.
  (blender/blender@83ce3ef0db).
- Changing UI font in Preferences will now start in your OS Fonts
  folder.
  (blender/blender@d3a2673cb8, blender/blender@048cece74d).
- File Browser List View removes columns and reformats as width is
  decreased.
  (blender/blender@07820b0703).
- Improved Color Picker cursor indication and feedback.
  (blender/blender@c11d5b4180).

![](../../images/Colorpicker.png){style="width:400px;"}

- Text Object fonts now look in the fallback stack when characters are
  not found.
  (blender/blender@604ee2d036).

![](../../images/Textobjectfallback.png){style="width:800px;"}

- Animation marker drawing improvements.
  (blender/blender@0370feb1bf).
- Improved corner rounding for menus and popup blocks.
  (blender/blender@42ddc13033).

![](../../images/Menurounding.png){style="width:400px;"}

- Improved quality of menu and popup block shadows.
  (blender/blender@0335b6a3b7).
- Improved initial display of compositor node trees.
  (blender/blender@ff083c1595).
- New Text Objects will use translated "Text" as default.
  (blender/blender@5e38f7faf0).

![](../../images/TranslatedText.png){style="width:800px;"}

- Eyedropper can now pick colors outside the Blender window on Mac.
  (blender/blender@639de68aaa).
- Open Recent menu items now show blender version and thumbnail if available.
(blender/blender@0b0e0601a1).
- Open Recent menu now includes "Clear Recent Files List" item.
(blender/blender@1ccc958150).
- Background Images can now be shown with render color transforms applied.
(blender/blender@29b1658124).
- File Browser tooltips now show blender version, image dimensions, video
details, etc. (blender/blender@cd4328dd82).

![](../../images/Tooltip_Details.png)

- Some changes and corrections to the Text Object "Special Characters" menu.
(blender/blender@0251701cd6).
- Clarify Liquid Diffusion/Viscosity Properties (blender/blender@67b21ce54d).
- The auto-save timer is now restarted after saving manually (blender/blender@f0f304e240).
- Dialog to enter characters by Unicode value into Text Objects. (blender/blender@6d357dc60d).
- Operator Properties dialogs now include "Cancel" button. (blender/blender@0d6aec1c21).
- All disclosure open/close items now using the same chevron-style icon. (blender/blender@8d48770418).
- Updated general Mask icon and Grease Pencil "Invert" icons. (blender/blender@f3c401ef6f).
- Added visual toggle for Sculpt and Grease Pencil automasking options. (blender/blender@c15d1b8ccb).

## Image Editor

- Image Editor now allows rotating images by 90 degree increments. (blender/blender@93562a1cc5).
- Image Vectorscope has updated look, and ability to display tinted or luma scope. (blender/blender@567455124d6).

![](../../images/41_image_vectorscope.png){style="width:800px;"}

## Node Editor

- The *Ungroup* operator now ungroups all selected group nodes instead
  of just the active one
  (blender/blender@f3cd25370c5a5eac4e4bf20c330caeba5d2d2d84).
- Socket picking when creating node links has been improved to reduce
  the number of mis-clicks
  (blender/blender@74dd1e044b55d10f6567697be2a30eda8995f697).

## Outliner

- Double-click on Outliner collection to select all children. (blender/blender@796577d76e).
- Modifiers can now be applied from the outliner (blender/blender@1c503c094c).
- Outliner context menu contains "Show Hierarchy" and "Expand/Collapse All".
  (blender/blender@4793b4592b, blender/blender@f815484e7d).

## 3D Viewport

- Walk mode now supports relative up/down (using R/F keys)
  (blender/blender@c62009a6ac961e199285dee0d7b5037132a067a7).
- Improved Mesh Edge Highlighting.
  (blender/blender@dfd1b63cc7).
- Improved contrast for text overlays. (blender/blender@a4a8683788).
- Shadowed text for Geometry Nodes Viewer attributes. (blender/blender@38e7b4e473).
- Gizmo button to toggle Lock Camera to View. (blender/blender@23faaac68b ).

## Linux

- Input Method Editors (IME) now supported for Wayland
  (blender/blender@a38a49b073f582a0f6ddcca392f2760afdc4d5ed).
