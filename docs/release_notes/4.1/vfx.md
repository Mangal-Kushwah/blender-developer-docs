# Blender 4.1: VFX & Video

## Compositor

### Added

- All nodes are now supported in the *Viewport Compositor*, including the newly added:
  - Vector Blur. (blender/blender@e84dc990b1)
  - Defocus. (blender/blender@e055db6605)
  - Cryptomatte. (blender/blender@a8e13994b8)
  - Keying Screen. (blender/blender@17bdf18397)

- A new *Split* node was added, which replaces the *Split Viewer* node with the exact same
  functionality, except it now provides its output as a node output. (blender/blender@153f14be2b)

- A new Size input to the Kuwahara node was added to allow variable sizing. Additionally, a new
  *High Precision* option was added to the node in the *Classic* mode, which produces more accurate
  results for high resolution and high dynamic range images. (blender/blender@203559757a,
  blender/blender@e4a93d7b8c)

- A new size property was added to the Pixelate node, which allows the node to be used without
  being surrounded by scale-down and scale-up nodes. (blender/blender@baab14ca38a,
  blender/blender@feb2d027094)

- A new Precision option was added to the node tree options. It allows choosing between full
  precision and automatic precision, where the automatic option uses half precision for the
  viewport and interactive compositing. Half precision runs faster with less memory usage, albeit
  with reduced precision and value ranges. (blender/blender@474b6fa070)

- A new Filter Type option was added to the *Map UV* node to allow the user to choose between
  Anisotropic filtering and Nearest Neighbour filtering. (blender/blender@a3b7674c6e)

### Changed

- The *Keying Screen* node was changed to use a *Gaussian Radial Basis Function Interpolation*,
  which produces smoother temporally stable keying screens. The node now also have smoothness
  parameter to control the width of the interpolation. (blender/blender@75c947a467)

![](../../images/KeyingScreenTriangulationVsRBF.png)

- The *Inpaint* node was changed to use Euclidean distance instead of Manhattan distance, resulting
  in a more uniform filled regions. The node now also uses a two-pass algorithm that allows better
  smoothing for internal filling, as demonstrated in the following example.
  (blender/blender@48d7d60c96)

![](../../images/compositor_41_new_inpaint_comparison.png)

- The *Bilateral Blur* and *Bokeh Blur* nodes now assume extended image boundaries, that is, they
  assume pixels outside of the image boundary are the same color as their closest boundary pixels.
  While they previously ignored such pixels and adjusted blur weights accordingly.
  (blender/blender@3d7e84f57d, blender/blender@a433adda8e)

- The *Double Edge Mask* node now include the zero point of the gradient, while it previously
  started from the gradient value after zero. (blender/blender@049b0e6539)

- The *Flip* node now works in local space, which means flipping will not change the location of
  the image if it was translated. (blender/blender@70a8a9e4d9)

- The *Crop* node no longer flips its bounds if they were inverted, which means that if the upper
  bound was less than the lower bound, the node will crop the image to nothing.
  (blender/blender@4bf08198a7)

- Scaling and Rotation in the Viewport Compositor are now immediately realized, which means scaling
  up an image will now actually produce more pixels, and that applying filters on rotated images
  will work as expected, instead of being applied along the direction of rotation.
  (blender/blender@e592763940)

- Translations in the Viewport Compositor are now immediately realized for the axes that has
  enabled wrapping. Consequently, the image will not get translated, but its content will, in a
  clip on one side, wrap on the opposite side manner. Further, wrapping information is no longer
  propagated to future automatic realizations, so tilling or repeating an image is no longer
  possible. An alternative method of repetition will be introduced in a later patch.
  (blender/blender@1500a594ad)

### Improved

- The Viewport Compositor now cache multi-pass images, making multi-pass compositing much faster.
  (blender/blender@356480fabb)
- The *Double Edge Mask* node is now orders of magnitude faster. (blender/blender@049b0e6539)
- The *Z Combine* and *Dilate* nodes now use an improved Anti-Aliasing algorithm.
  (blender/blender@1aafb6802b)
- The *Defocus* node now uses a more accurate method to compute the bokeh radius, so results should
  now match render engines better. (blender/blender@65e1a3a5c3)
- The *Sun Beams* node now produces smoother results. (blender/blender@cbb738191e)
- The compositor now only executes if its result is really used or viewed.
  (blender/blender@e165624885)

### Removed

- The *Split Viewer* node was removed, replaced by the *Split* node. (blender/blender@153f14be2b)

## Sequencer

### Performance

The video Sequencer got many performance optimizations across the board.

- Timeline user interface repaints 3x-4x faster for complex timelines.
(blender/blender@df16f4931e)
- Effects: Glow is 6x-10x faster (blender/blender@fc64f48682), Wipe is 6x-20x
faster (blender/blender@06370b5fd6), Gamma Cross is 4x faster
(blender/blender@9cbc96194e), Gaussian Blur is 1.5x faster
(blender/blender@5cac8e2bb4), Solid Color is 2x faster (blender/blender!117058).
- Various parts of image transformation (blender/blender@1e0bf33b00, blender/blender!117125), movie
frame reading and writing (blender/blender@422dd9404f,
blender/blender@4ef5d9f60f), color management (blender/blender@f3ce0645e4) and
audio resampling (blender/blender@986f493812) were sped up.
- Luma Waveform display calculation is 8x-15x faster
(blender/blender@93ca2788ff).

### Filtering

Image/movie filtering that is done when scaling/rotating strips has been improved.

- Default strip filter is now "Auto", which automatically chooses the most appropriate
  filter based on scaling factors (blender/blender!117853):
  - No scale/rotation and integer positions use Nearest,
  - Scaling up by more than 2x uses Cubic Mitchell,
  - Scaling down by more than 2x uses Box,
  - Otherwise Bilinear.
- Strip transforms got Cubic filter option, previously it only
  existed in Transform Effect strip (blender/blender!117100, blender/blender!117517).
  Cubic filter exists in B-Spline (matches cubic elsewhere in Blender) and Mitchell
  (usually better for images) varieties. Cubic filtering is also faster now.
  ![](../../images/VSE41CubicFilters.png)
- Bilinear filter no longer adds a transparent edge near image border when scaling it up.
  (blender/blender!117717)
- Subsampled3x3 filter was replaced by a more general Box filter, that better handles
  scaling images down by more than 3x. (blender/blender!117584)
- Various "off by one pixel" issues resulting in gaps between neighboring strips,
  or images being shifted by half a pixel, have been solved. (blender/blender!116628)

### Scopes

Sequencer Scopes got visual look improvements (blender/blender!116798):

![Histogram](../../images/VSE41ScopeHistogram.png){style="width:800px;"}
![Waveform (Luma)](../../images/VSE41ScopeWaveform.png){style="width:800px;"}
![Waveform (Parade)](../../images/VSE41ScopeParade.png){style="width:800px;"}
![Vectorscope](../../images/VSE41ScopeVecscope.png){style="width:800px;"}

### Audio

- Audio waveforms are now displayed by default, and
  got a display option to show upper half of the waveform only.
  (blender/blender@a95dd8438d) (blender/blender@1be8b51b11)
