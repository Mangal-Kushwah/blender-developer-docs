# Blender 2.93: Modeling

## Curves

- New option *Taper Mode* to control how taper objects affect the
  effective radius of the spline. The different taper modes are shown
  below on a spline with a radius of zero at one end and a radius of one
  at the other end.
  (blender/blender@ec97450a)

Taper Mode Override.png\|thumb\|Override mode. Taper Mode
Multiply.png\|thumb\|Multiply mode. Taper Mode Add.png\|thumb\|Add mode.

## Transform

- Proportional edit connected mode now has more accurate falloff using
  geodesic distances. Previously this gave mesh topology dependent
  results that were not smooth.
  (blender/blender@21b9231)

## Subdivision Surface

The list of available options for UV smoothing has been extended
(blender/blender@66151b5de3ff).
The available options now are:

- None - UVs are not smoothed, boundaries are kept sharp.
- Keep Corners - UVs are smoothed, corners on discontinuous boundary are
  kept sharp.
- Keep Corners, Junctions - UVs are smoothed, corners on discontinuous
  boundary and junctions of 3 or more regions are kept sharp.
- Keep Corners, Junctions, Concave - UVs are smoothed, corners on
  discontinuous boundary, junctions of 3 or more regions and darts and
  concave corners are kept sharp.
- Keep Boundaries - UVs are smoothed, boundaries are kept sharp.
- All - UVs and boundaries are smoothed.

Additionally, the default has been changed to "Keep Boundaries"
(blender/blender@3d3b6d94e6e),
helping to avoid UVs distortion.

## Mirror Modifier

A new option has been added to the mirror modifier: Bisect Distance
(blender/blender@00ec99050ea4).
This parameter controls the distance from the mirror plane within which
vertices are deleted during the bisecting process. Note that this is
most useful when working at a small scale or with vertices close to the
mirror plane. The default value (`0.001`) will work fine for most
cases.

Bisect distance small.png\|thumb\|Bisect distance 0.001. Bisect distance
large.png\|thumb\|Bisect distance 0.01.

## General Changes

- The order was changed for items in the edit mode merge operator to
  keep shortcuts consistent
  (blender/blender@bc0e121bce257a).
- Edge loop select now supports loops where each edge has 3 or more
  connected faces
  (blender/blender@493628e5418b1543d82e314b34832ad6cc365055).
