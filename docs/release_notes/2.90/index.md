# Blender 2.90 Release Notes

Blender 2.90 was released on August 31, 2020.

Check out the final [release notes on
blender.org](https://www.blender.org/download/releases/2-90/). List of
[corrective
releases](corrective_releases.md).

## [User Interface](user_interface.md)

## [Modeling](modeling.md)

## [Sculpt](sculpt.md)

## [Grease Pencil](grease_pencil.md)

## [Cycles](cycles.md)

## [EEVEE](eevee.md)

## [Import & Export](io.md)

## [Python API](python_api.md)

## [Physics](physics.md)

## [Animation & Rigging](animation_rigging.md)

## [Virtual Reality](virtual_reality.md)

## [More Features](more_features.md)

## [Add-ons](add_ons.md)

## Compatibility

- The minimum required macOS version was changed from 10.12 Sierra to
  10.13 High Sierra. Apple extended support for Sierra ended in October
  2019.

## [Corrective Releases](corrective_releases.md)
