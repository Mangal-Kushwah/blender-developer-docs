# Modeling

### Curve Pen Tool

- A new "Curve Pen" tool in curve edit mode can be used to rapidly edit
  and create curves
  (blender/blender@336082acba51).
  - The tool supports quickly adding, deleting, and tweaking control
    points.
  - Much more functionality is available with various shortcuts, and
    editable in the keymap for further customization.

### Multi-User Data Object

- When trying to apply a modifier to a multi-user data object users can
  choose to make it single user, instead of failing to apply
  (blender/blender@35f34a3cf840).
- For applying object transform the new option allows to isolate the
  selected multi-user data objects from the others if needed
  (blender/blender@8621fdb10dc4).

<video src="../../../videos/Apply-transforms.webm" width="720" controls=""></video>

### General

- The "Select Similar" operator for Meshes now supports selecting by
  similar vertex crease values
  (blender/blender@8abd8865d2e4743035eedad21a72c92d70474907).
- NURBS curves knots generation has been improved
  (blender/blender@0602852860dda7).
  - "Cyclic" can be used with other knot options.
  - "Endpoint" and "Bezier" can be used at the same time.

![A scheme showing NURBS with all built-in knots generation modes. On the sides, knot structures are visualized. Diagonal lines show by what knots particular control point is affected. Blue and cyan arrows show to which control points the curve is clamped and which knots create that clamp. Purple line demonstrates where cycle in knot begins.](../../images/NURBS_Knots_Generation_3.2.png)

### Modifiers

- The Vertex Weight Mix modifier now supports Minimum and Maximum as
  operations that can be applied to combine input weights
  (blender/blender@fa715a158a4c4).
