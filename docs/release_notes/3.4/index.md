# Blender 3.4 Release Notes

Blender 3.4 was released on December 7, 2022.

Check out out the final [release notes on
blender.org](https://www.blender.org/download/releases/3-4/).

## [Animation & Rigging](animation_rigging.md)

## [Core](core.md)

## [EEVEE & Viewport](eevee.md)

## [Grease Pencil](grease_pencil.md)

## [Modeling & UV](modeling.md)

## [Nodes & Physics](nodes_physics.md)

## [Pipeline, Assets & I/O](pipeline_assets_io.md)

## [Platform-Specific Changes](platforms.md)

## [Python API & Text Editor](python_api.md)

## [Render & Cycles](cycles.md)

## [Sculpt, Paint, Texture](sculpt.md)

## [User Interface](user_interface.md)

## [Add-ons](add_ons.md)

## Compatibility

- The "Transfer Attribute" node has been split into three separate
  nodes. For more information, see the [geometry nodes
  section](nodes_physics.md#geometry-nodes)
- The "MixRGB" node for Shader and Geometry Nodes is converted to the
  new "Mix Node" on loading. Files saved with the new node are not
  forward compatible with older versions of Blender
  (blender/blender@bfa0ee13d539).
- Because of asset support in node editor add menus, node group asset files
  must be re-saved in version 3.4 (blender/blender@bdb57541475f).

## [Corrective Releases](corrective_releases.md)
