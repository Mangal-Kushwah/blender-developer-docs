# Blender 2.80: Grease Pencil

## 2D Animation

Grease pencil is now a full 2D drawing and animation system.

Grease pencil objects are a native part of Blender, integrating with
existing object selection, editing, management, and linking tools.
Strokes can be organized into layers, and shaded with materials and
textures. Besides a draw mode for strokes, these objects can also be
edited, sculpted and weight painted similar to meshes.

Modifiers can be used to deform, generate and color strokes. Often used
mesh modifiers like array, subdivide and armature deform have
equivalents for strokes. Rendering effects like blur, shadows or rim
lighting are also available. Compared to modifiers, these do not work on
the stroke but on the rendered pixels of each object.

For more information, see the [User
Manual](https://docs.blender.org/manual/en/2.80/grease_pencil/introduction.html).

![](../../images/Blender2.8_grease_pencil_hero.jpg){style="width:600px;"}

## Annotations

The old Grease Pencil has been renamed to Annotations, and streamlined
to be used just for adding annotations to scenes rather than creating
artwork. The annotation tool in the toolbar is now the main entry point
for making annotations.
