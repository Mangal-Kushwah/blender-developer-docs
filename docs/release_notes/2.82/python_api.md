# Blender 2.82: Python API

## API Changes

- The Alembic exporter operator `bpy.ops.wm.alembic_export()` changed
  the keyword argument `visible_layers_only` to
  `visible_objects_only`. The semantics have not been changed.
  (blender/blender@d9e61ce1953b)

## Curve Profile

The `CurveProfile` type was added which allows the construction and
evaluation of a 2D curve.
(blender/blender@ba1e9ae4733a)

The widget supports arbitrary curves rather than an X to Y mapping, so
the result of evaluation is a 2D vector.

## Python Environment

Blender's embedded Python now ignores environment variables such as
`PYTHONPATH`, pass the argument `--python-use-system-env` to enable
Python environment variables.
(blender/blender@7c2f0074f3fe2411daa7a6e351d7cbc535246871)
