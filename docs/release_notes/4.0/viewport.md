# Blender 4.0: 3D Viewport

## Overlays

- New preference to toggle
  [fresnel](https://docs.blender.org/manual/en/4.0/editors/preferences/viewport.html#display)
  in Edit Mode.
- [Wire
  Color](https://docs.blender.org/manual/en/4.0/editors/3dview/display/shading.html#viewport-shading)
  is now available for all shading modes.
- Object/mode-specific overlays have been moved into their own popover.
  (blender/blender@4fa4f5432d)
- Statistics: Ability to see per-item, and per-selection counts while in
  Object mode
  (blender/blender@6e20beba22).

![](../../images/ObjectStats.png){style="width:400px;"}

## User Interface

- Canvas selector in the header in paint modes.
  (blender/blender@3b5df8a7ea)

<!-- -->

- Tool settings headers in the 3D View and Image Editor draw with a
  fully transparent background (with the region overlap preference
  enabled) and the theme's region background behind clusters of buttons
  (blender/blender@55eaa755e3fcb0b9cb3417afee6f3e65b2c43d7b,
  blender/blender@ff78d33bb4eed30e667fcd42f88368b253ac238c).

![](../../images/Release_notes_4.0_transparent_tool_settings.png){style="width:800px;"}

- The 3D Viewport background is now set to single-color in the default
  theme, since the vignette effect can lead to glitches in certain
  hardware according to community feedback
  (blender/blender@eef2b61e9)
- 3D viewport header can now be made transparent (along with tool
  header) with theme color alpha change
  (blender/blender@d308f35896).

<!-- -->

- Tweaks to the Object Types visibility popover
  (blender/blender@7405993cb7).

![](../../images/ObjectVisibility.png){style="width:400px;"}

- Status text for modal operators now shown in the Tool Settings bar if
  visible
  (blender/blender@ca00c9aa3e).
- The Options panel accessed in Object mode and Mesh Edit mode has been
  reorganized.
  (blender/blender@560e9c654b)

## Other

- Text object selection can now start outside of the text block.
  Improved selection with text boxes and when on curves
  (blender/blender@5b3ce7b740).
- Numpad Return key can add new line while editing Text Objects.
  (blender/blender@ff6b25a200).
- Walk Navigation: New modal keymap to adjust jump height
  (blender/blender@f418e4f648d6543d3e4fc49d1174aab2772aa2fd).
