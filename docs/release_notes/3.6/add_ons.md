# Add-ons

## Storypencil

- It is now possible to create many scene strips at once.
  blender/blender-addons@27970e4b8b556d99cf3476e52c52d2d00628092d
- Support to render meta strip as one video. Previously, a video was
  generated per scene in the meta strip.

## New: VDM Brush Baker

A new add-on to easily create VDM brushes has been added. Previously
available as part of [a demo
file](https://www.blender.org/download/demo-files/#sculpting).

![](../../images/Vdm_brush_baker_screenshot.png)

## New: 3DS I/O

Support for importing and exporting the legacy `3ds` format is back
since it is still used by many applications and was missed by many
Blender users. (blender/blender-addons!104493)

Animations and almost all 3ds definitions can be imported and exported.
(blender/blender-addons@2dc2c1c3e4,
blender/blender-addons@6525a57a32,
blender/blender-addons@febc4b4433,
blender/blender-addons@859a2c1b9a)

All backporting changes are listed in the [commit details](https://projects.blender.org/blender/blender-addons/commit/c7a4b16aed).

## Collection Manager

Ported the QCD Move Widget to solely use the `gpu` API for drawing,
replacing the deprecated `bgl` API.
(blender/blender-addons@be6cbe0222)

### Bug Fixes

- Fixed the QCD Move Widget not dragging.
  (blender/blender-addons@14595effe5)
- Fixed an error when toggling off the only QCD slot that contains the
  active object.
  (blender/blender-addons@bee6a356c6)
- Fixed an error when restoring disabled objects.
  (blender/blender-addons@06328e8aea)
