# Blender 2.83: Animation & Rigging

## Weight Paint

- Bones corresponding to locked weight groups are now shaded red in
  weight paint mode.
  (blender/blender@966383138a)
- Added a pie menu for easily locking and unlocking vertex groups via
  bone selection (K shortcut key).
  (blender/blender@a1e50cfe6b)
- Added a new 'Lock-Relative' option to present weights as if all locked
  groups were deleted, and the remaining deform groups were
  re-normalized.
  (blender/blender@084bf7daee)

## Miscellaneous

- Action Baking now creates keyframes on all frames both for new actions
  and baking to an existing action. Previously this was only done for
  new actions.
  (blender/blender@ded7af53b4f2)
- Child Of constraint has a different method of computing the inverse.
  Because of this, Set Inverse will always set the correct inverse
  matrix (previously it only worked reliably when all the checkboxes
  were checked).
  (blender/blender@10162d68e38)
  (blender/blender@464752876fb)
- Scene Audio Volume can be animated. This was possible in Blender 2.79
  and earlier, but has been broken since 2.80, and is fixed in 2.83.
  (blender/blender@6adb254bb04)
