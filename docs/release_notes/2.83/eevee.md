# Blender 2.83: EEVEE

## Render Passes

More render passes supported for compositing.
(blender/blender@be2bc97eba)
([developer docs](../../features/eevee/render_passes.md))

- Emission
- Diffuse Light
- Diffuse Color
- Glossy Light
- Glossy Color
- Environment
- Volume Scattering
- Volume Transmission
- Bloom
- Shadow

## Light Cache Update

Reflection Probes are now stored in cubemap arrays instead of octahedron
maps arrays. This means pre-2.83 files will need to be rebaked and light
caches from 2.83 will not be loadable in previous blender versions.
(blender/blender@7dd0be9554ae)

|Before|After|
|-|-|
|![](../../images/Cubearray_before.png){style="width:340px;"}|![](../../images/Cubearray_after.png){style="width:300px;"}|

## Hair Transparency

Hair geometry now supports blending with alpha hash and alpha clip mode.
Shadow blend modes are now also supported.
(blender/blender@035a3760afd2)

![](../../images/Hair_alpha.png){style="width:340px;"}

## Other Changes

- A new `High Quality Normals` option has been added to the render
  settings to avoid low res normal issues on dense meshes.
  (blender/blender@e82827bf6ed5)
- A new `Half Float Precision` option has been added to the Image
  datablock settings adding the possibility (when the option is turned
  off) to use 32 bits floating point precision when loading float
  textures.
  (blender/blender@4e9fffc28926)
- Sun shadow bias have been fixed and now behave the same as point light
  shadow bias. User intervention may be required to fix certain scenes.
  (blender/blender@31ad86884cb1)
- Lookdev now have an option to display the world environment with
  variable amount of blurring.
  (blender/blender@6e23433c1a74)
