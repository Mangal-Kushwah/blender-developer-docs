# Blender 3.1: Pipeline, Assets & IO

## Alembic

- Animated vertex colors can now be exported to Alembic
  (blender/blender@4e2478940eb8).
- Reading override layers is now supported
  (blender/blender@0a08ac2528e).
  Override layers allow to override data (attributes, animations,
  topology, etc.) from Alembic caches as long the hierarchies inside of
  the original file and the layer(s) match.

![User Interface for the Alembic override layers](../../images/Override_layers_ui.png)

<figure>
<video src="../../../videos/Abc_layers.mp4" title="Video demonstration of updating an animation using layers." controls=""></video>
<figcaption>Video demonstration of updating an animation using layers.</figcaption>
</figure>

## Asset Browser

- Asset libraries are indexed for faster browsing
  (blender/blender@dbeab82a89,
  blender/blender@36068487d0).
- Node groups can be added to asset libraries, and support drag & drop
  into the node editor
  (blender/blender@37848d1c8edf18ac52095cec).

## Image I/O

- Enabled support for OpenEXR DWAB compression
  (blender/blender@b1bd0f8ffdaf)

## Obj I/O

- A new C++ version of the .obj exporter is provided as a replacement
  for the Python addon
  (blender/blender@4e44cfa3d996)

Some speed comparisons:

|Scene|File Size (MB)|Time Old (s)|Time New (s)|Speed-up|
|-|-|-|-|-|
|Default cube: 9 subdivisions|365|132|14|9.4x|
|Default cylinder: 2038 copies|20|12.3|0.9|13x|

- For updates regarding the python add-on, please check the [Add-ons page](add_ons.md#obj-i/o).

## USD

### Exporter

- Export USD Preview Surface shaders from simple Principled BSDF node
  networks and allow exporting textures referenced by materials to a
  textures directory next to the USD file
  (blender/blender@c85c52f2ce478ab0e30c5e93fd5a5cb812db232f)

## glTF 2.0

### Importer

- Implement a user extension system
  (blender/blender-addons@a5205b0289717dc418c1a6070c89039204a2e951)
- Performance enhancement when images are packed
  (blender/blender-addons@c2eeb9bb4732602c324a011f7f913a33a5e8b1f2)
- Use relative path when possible
  (blender/blender-addons@4d2d118f9add7f634d6c2d7428f4c4d307c21368)
- Set empty display size, based on distance parent/children
  (blender/blender-addons@ec45566f05f0bebad99d2fddd1c2f2815edf9a4e)
- Fix invalid index mesh name
  (blender/blender-addons@c8445b67fc2dd3696c393c5dc8cdba0da6a911c3)

### Exporter

- Manage tweak mode in NLA when needed
  (blender/blender-addons@f35aff344ff651364a54cc6c9fa75bf2699ad2a8)
- Better texture image management
  (blender/blender-addons@97d4577285f5f7a75f663559a6a3ad6c692c087a,
  blender/blender-addons@38be61ebb868c0b56c140286af8720b2e7d0de11,
  blender/blender-addons@38be61ebb868c0b56c140286af8720b2e7d0de11)
- Fix regression when exporting armature object animation with only 1
  key
  (blender/blender-addons@f1c4959cd05682855c6bfa0e96924f0b6a2676aa)
- Fix back compatibility for use_selection
  (blender/blender-addons@503570ad703268ef55cfa38beaa28ad15e3ecc57)
- Avoid issue with setting frame with python v \>= 3.10
  (blender/blender-addons@a0d1647839180388805be150794a812c44e59053)
- Use custom range on action
  (blender/blender-addons@19385dbc57f566e27914a361768d3aa6dad95ec6)
- Remove proxy management
  (blender/blender-addons@6da09c309d756cecac66ac27257414c36f9b51b7)
- Manage factors for Clearcoat extension
  (blender/blender-addons@dc2d9b0864daf6805c89632004f8c98c91cc311b)
- Workaround for image size copy not stored
  (blender/blender-addons@3b088fc33dc62950887977ee3ecd0ce01d03ebd3)
- Option to not export texture images
  (blender/blender-addons@4003baf03d7d6d78ef4c16be79c73edadf87759c)
- New animation hooks
  (blender/blender-addons@bfcf35f7464b9445322b2ba3bc8214339bd91317)
- Optimized anims can now be disabled
  (blender/blender-addons@b2adbc6ba56d9e4380325866b8a3ae6d2d907a39)
