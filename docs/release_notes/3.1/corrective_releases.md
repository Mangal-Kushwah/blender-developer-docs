# Blender 3.1.2

Released on Apr 1st 2022, and a followup to 3.1.1 \[which unfortunately
introduced a regression\], Blender 3.1.2 features more bug fixes:

- Regression: The location of "Viewport Gizmos" is not correct in POSE
  mode (multi-object/armature editing, multiple bones selected).
  [T96347](http://developer.blender.org/T96347)
- Compositor: Missing output UI for Normal node.
  [T96860](http://developer.blender.org/T96860)
- GPU subdivision: Crash on select in Edit Mode with X-Ray on and Mirror
  Modifier. [T96344](http://developer.blender.org/T96344)
- GPU subdivision modifier: Smooth Shade doesn't work.
  [T96915](http://developer.blender.org/T96915)
- OBJ export: mark the new 3.1+ exporter as experimental, and
  reintroduce menu item for the old exporter.
  \[blender/blender@2cfca7d9101,
  blender/blender-addons@19337ef729\]

# Blender 3.1.1

Released on Mar 30th 2022, Blender 3.1.1 features many bug fixes:

- Fix memory leak evaluating PyDrivers.
  blender/blender@eff7e9aa45a
- PyAPI: ID property group returns wrong type with iter().
  [T94121](http://developer.blender.org/T94121)
- Grease Pencil \> Stroke \> Normalize Thickness Causes Crash.
  [T96352](http://developer.blender.org/T96352)
- GPencil: Fix unreported select error in Normalize operator.
  blender/blender@af6b88c629d
- Unable to set active material output node using Python.
  [T96292](http://developer.blender.org/T96292)
- is_active_output stopped working correctly.
  [T96396](http://developer.blender.org/T96396)
- Nodegroups don't update the shaders anymore in 3.1.
  [T96402](http://developer.blender.org/T96402)
- Crash with Close Area menu 3D view.
  [T96416](http://developer.blender.org/T96416)
- Issue clicking on stem of arrow gizmos to scale on axis.
  [T96357](http://developer.blender.org/T96357)
- Armature corrupted after undo. [T96452](http://developer.blender.org/T96452)
- Overlapping Volumes broken in Cycles, GPU only (possible regression).
  [T96381](http://developer.blender.org/T96381)
- Multiscatter GGX regression with non-zero roughness and a bump texture
  on a non-manifold object. [T96417](http://developer.blender.org/T96417)
- Crash when changing shader to nodegroup in properties.
  [T96386](http://developer.blender.org/T96386)
- GPencil: Fill freezes when use invert and click inside areas.
  [T96518](http://developer.blender.org/T96518)
- Fix object centers & geometry selecting meta-elements in edit-mode.
  blender/blender@6aa45434623
- Stereoscopy with Motion Blur crashes in Eevee.
  [T96572](http://developer.blender.org/T96572)
- C3D Import. [T14392](http://developer.blender.org/T14392)
- Crash when a curve object got an array modifier with a mesh object
  containing a vertex group set as cap.
  [T96494](http://developer.blender.org/T96494)
- Bake normals for multi-resolution object is broken.
  [T96401](http://developer.blender.org/T96401)
- Set ID for Instances not work in Blender 3.1.
  [T96420](http://developer.blender.org/T96420)
- Regression: Can not translate after selecting with Select Circle.
  [T96515](http://developer.blender.org/T96515)
- Regression: NLA crash when reordering tracks if no object is selected.
  [T96624](http://developer.blender.org/T96624)
- Crash on Geometry Nodes Edit Mode tap Tab.
  [T96316](http://developer.blender.org/T96316)
- Regression: Texture Mapping properties of texture nodes are not
  updated in 3D Viewport. [T96361](http://developer.blender.org/T96361)
- Regression: Crash when pressing F3 outside a Blender window if
  Developer extras is on. [T96705](http://developer.blender.org/T96705)
- snap does not work properly. [T96711](http://developer.blender.org/T96711)
- UV Editor doesn't work when GPU Subdivision in the Viewport is
  enabled. [T96372](http://developer.blender.org/T96372)
- Fix missing updates for external render engines rendering tiles.
  blender/blender@c91d39e4735
- Add Curve Extra Objects addon fails when adding Curvy Curve.
  [T96342](http://developer.blender.org/T96342)
- Regression: Script using bmesh.ops.wireframe and bmesh.ops.bevel does
  not work the same in 3.1 and in 3.01.
  [T96308](http://developer.blender.org/T96308)
- Regression: Crash when executing
  bpy.types.ShapeKey.normals_vertex_get.
  [T96294](http://developer.blender.org/T96294)
- Fix text editor failure to move the cursor for syntax errors.
  blender/blender@694e20b1415
- Gpencil: Inverted Fill makes extra stroke at origin (0,0,0).
  [T96790](http://developer.blender.org/T96790)
- Image editor: not updating after image operation.
  blender/blender@f6e2c9bc97d
- Regression: Blender 3.1 bake from multires not reflected in the Image
  Editor. [T96670](http://developer.blender.org/T96670)
- Regression: GPencil primitives handlers not working.
  [T96828](http://developer.blender.org/T96828)
- Regression: Snapping is broken with proportional editing.
  [T96812](http://developer.blender.org/T96812)
- Regression: Cutting a strip with keyframes in the VSE deletes the
  keyframes from the original (left) strip.
  [T96699](http://developer.blender.org/T96699)
- Regression: VSE Wipe transition does not work.
  [T96582](http://developer.blender.org/T96582)
- Heap corruption in `file_browse_exec`.
  [T96691](http://developer.blender.org/T96691)
- GPencil: Scripting `weight_get` cannot retrieve weights of certain
  vertex groups. [T96799](http://developer.blender.org/T96799)
- vertex paint face selction display bug gpu subdivision.
  [T96356](http://developer.blender.org/T96356)
- New OBJ exporter fixes:
  - Reintroduce setting presets and an option to skip modifiers.
    [T96303](http://developer.blender.org/T96303)
  - Fix wrong normals on non-uniformly scaled objects.
    [T96430](http://developer.blender.org/T96430)
  - Fix scaling factor not being applied correctly.
    [T96415](http://developer.blender.org/T96415)
  - Fix export of "material groups" option.
    [T96470](http://developer.blender.org/T96470)
  - Fix issues with some apps (e.g. Procreate) with multiple materials;
    the new exporter was not grouping faces by material.
    [T96511](http://developer.blender.org/T96511)
