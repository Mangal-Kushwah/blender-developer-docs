# Release Branch Merge Playbook

## How to handle merge/commit scenarios

### Fix committed to main, should have been in release branch

- `git cherry-pick` commit from main in release branch.
- Merge release branch to main as usual.

### Someone forgot to merge release branch to main

- Merge yourself if there are no conflicts, or conflicts are easily
  resolved.
- Otherwise poke the developer to merge to main.

### Bugfix has unintended changes

- If the issue is critical (e.g. crashes in common scenario, data
  corruption), revert immediately.
- If not critical:
  - You can take a few hours to find a fix if you have time.
  - Otherwise revert.
- Remember to re-open any resolved issues when reverting.

### Oops, I merged main into the release branch

- Notify \#blender-coders, get an experienced developer to help.
- The strategy to revert will be:
  - Revert the merge commit in release branch, carefully check git log
    and diff, then push to release branch.
  - Merge release branch into main.
  - Revert the revert commit so main is again in state it should be.
