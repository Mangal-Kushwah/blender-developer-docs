# Blender Developer Handbook

Welcome to the Blender Developer Handbook.

This handbook contains all general documentation about Blender development. That
is, information that is not specific to some project, module or feature. It is
an important resource for both new, and experienced developers.
