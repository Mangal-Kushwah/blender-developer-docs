# Pick a Project

Once you have blender compiled you're probably wondering where to start
tinkering. If you are a Blender user then there is probably some
niggling thing you would like to add or change. If you are new to
Blender then you will need to take a look and see what you might work
on.

There are no hard and fast rules about where to start, so find an area
that interests you and start looking into it.

Some hints for your first projects:

- An area of interest (even better, an area of Blender you use)
- Double check it is not already done.
- Try to avoid a project that spans across many files and areas of
  Blender, since you may get bogged down trying to understand everything
  at once.

Here are some areas that can give you a foot in the door:

## Small Improvements

Adding in small features or making small improvements is a good
way to start getting your hands dirty, even if you don't contribute
the changes. The feature you worked on can evolve into something more
useful, take a new direction, or spark interest in new areas to develop.

There is a [list of quick hacks](https://projects.blender.org/blender/blender/issues?labels=302) -
tasks that core developers think could be reasonably tackled as a first
blender project without a major time commitment. These tasks typically
would take a core developer less than 4 hours to accomplish, but might
take quite a bit more time for a new coder who needs to learn the code
base.

When using Blender you might run into some simple limitation - Lasso select
tool didn't work on UV editor for example, or that the smooth tool doesn't
work on a lattice. Making improvements like this is good because you're
working within the current design and there's a much higher chance of
having your work accepted.

## Bug Fixing

One of the easiest ways to get involved with coding Blender is by fixing
bugs. Bug fixing lets you focus on a small part of the Blender source
rather than trying to understand it all at once. The list of current
bugs is on [projects.blender.org](https://projects.blender.org/blender/blender/issues).

Bug fixes may range from trivial to highly complicated. Recently reported
bugs tend to be simpler to resolve.

So pick an area of interest and start fixing! When you have got it,
[make a pull request](../contributing/pull_requests.md).

If you can't find a fix, posting your finding to the bug report can still
help others make progress.

Using a debugger helps investigating crashing and stepping through the
code. See the [tooling](../tooling/index.md) page for some information
about debugging tools.

## Janitor Work

Some more mundane tasks can also be helpful and a good way to get your
first patch accepted.

- Tooltip or spelling corrections.
- Fixes and improvements to the build system.
- Quiet compiler warnings.

## Advice

- In most cases we suggest to start really small and treat it as an
  exercise, your first project may not end up being useful and even
  things you would expect to be *easy* might not be.
  This also helps you become used to navigating the source code, reading
  it, making edits - and understanding it. While you do this you can
  think of more ambitious/interesting things to change too, so this time
  isn't wasted.

- Avoid small patches which only tweak existing behavior or tweaks to
  defaults *(early on at least)*. These kinds of changes are very easy
  for existing developers to make, so it's not really all that helpful
  to send such *opinionated* patches.

  It's common for people to come to Blender from other software and want
  to make it work how they like, but it's not the purpose of Blender to
  be a clone of another application.
  At least learn *the Blender way* before trying to make Blender behave
  like some other application, perhaps what you want can be done with
  key-map modifications, or added as a user preference, but to start
  with it's best to avoid *controversial* changes.

- For a definitive answer on whether your project might be accepted -
  [get in contact](contact.md#contribution-process)
  with the developers who would reviewer your work.
