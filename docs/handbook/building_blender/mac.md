# Building Blender on macOS

Building Blender for macOS needs a bit of preparation. However the steps
are not very complicated and if you follow the instructions carefully
you should be able to build Blender.

macOS version 11.2 and newer is supported.

## Quick Setup

### Install Development Tools

Install either:

- Xcode from the App Store, to get a full development environment.
- Xcode command line tools by running this in the terminal. This takes
  up less disk space.

``` bash
xcode-select --install
```

### Install CMake and Git LFS

CMake and Git LFS must be installed and available to be used by
Blender.

There are multiples ways to install them. We recommend using
[Homebrew](https://brew.sh/). Follow the install instructions on the
Homebrew website, then run in the terminal:

``` bash
brew install cmake git git-lfs
```

Other package managers like MacPorts and Fink work as well. You may also
install CMake and Git LFS manually. This is more advanced and
requires ensuring `cmake` and `git-lfs` commands are in the `PATH`.

### Download Sources and Libraries

Now you are ready to get the latest Blender source code from
Blender.org's Git repository. Copy and paste the following instructions
into a terminal window. These will create a blender-git folder off your
home directory and download the latest source code, as well as add-ons
and external libraries.

``` bash
mkdir ~/blender-git
cd ~/blender-git
git clone https://projects.blender.org/blender/blender.git
cd blender
make update
```

### Building

The easiest and fastest solution is to build Blender with the default
settings, and simply run this in the Terminal:

``` bash
cd ~/blender-git/blender
make
```

After the build finished, you will find `Blender.app` ready to run in
`~/blender-git/build_darwin/bin`.

### Updating

If you later want to update to and build the latest version, run:

``` bash
cd ~/blender-git/blender
make update
make
```

If building fails, it sometimes helps to remove the
`~/blender-git/build_darwin` folder to get a completely clean build.

## Branches

With the quick setup instructions the `main` branch will be built by
default, which contains the latest Blender version under development.

To build another branch or a [pull
request](../contributing/pull_requests.md), first checkout the branch and
then run `make update` to update the add-ons, libraries and tests to
match.

``` bash
cd ~/blender-git/blender
git checkout <branch-name>
make update
make
```

Depending on how big the changes are compared to the `main` branch, it
may be necessary to remove the build folder
`~/blender-git/build_darwin`. Otherwise the build folder may still
refer to libraries from another branch.

## Xcode

See the [Xcode development environment](../development_environments/xcode.md)
page for information on how to set up an Xcode project.

## Cross Compiling

On a machine with Apple Silicon, by default a version optimized for that
CPU architecture will be built.
It is also possible to build an Intel version, as follows.

```
./build_files/utils/make_update.py --architecture x86_64
make BUILD_DIR=../build_x86_64 BUILD_CMAKE_ARGS="-DCMAKE_OSX_ARCHITECTURES=x86_64"
```

