# Cycles Multi Device Scheduling

## Overview

When rendering on multiple devices, the image is horizontal in even
parts. As more samples are rendered, the size of each part is rebalanced
according to the performance of the device, and render buffers are moved
between devices as needed.

This scheduling algorithm is to be improved in the future, as load
balancing can be poor with few samples or in interactive rendering. For
long offline renders, or repeated viewport renders, rebalancing works
well.

## Details

TODO

## Possible Improvements

- Render alternating scanlines or strips on different devices, for even
  distribution in interactive rendering without rebalancing, at the cost
  of coherence.
- Find a better balancing algorithm for low number of samples when
  devices have very different performance.
