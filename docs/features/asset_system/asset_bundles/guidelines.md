> WARNING: **Work in Progress.**
> This page is a Work In Progress, and its contents are subject to
change as the Blender Assets project is further developed.

# Blender Asset Bundle info
  
✍ Authors: [Julian Eisel](https://projects.blender.org/JulianEisel/),
[Metin Seven](https://archive.blender.org/wiki/2024/wiki/User:Metin_Seven.html)

📅 Updated: **April 21, 2021**

## Introduction

The Blender Asset Bundle is meant to be a basic set of building blocks
to get started, not a full blown asset library with thousands of large
textures and heavy models.

Assets can exist of various data types, ranging from materials to
objects, and should be marked for use with the Blender Asset Manager,
including tags and a description.

Online asset platforms (markets or sharing sites) stay relevant for the
Blender artist ecosystem, and users can create and save their own assets
as well. The Asset Browser tries to bring a simple and streamlined
experience for that.

## Target audience

The primary target audience for bundled assets are beginning and
occasional Blender users. The Asset Bundle is a great way to help them
get started with content creation.

However, a good set of default assets can be helpful for more expert
level users as well, e.g. for quick scene mock-ups.

## Project organization

- **The Asset Bundle is [maintained in
  SVN](https://svn.blender.org/svnroot/bf-blender/trunk/lib/assets/).**
- **The Asset Bundle inventory is administered in the related Blender
  Asset Bundle Phabricator task:
  [T55239](http://developer.blender.org/T55239).**

  
A selection of other Blender artists can be approached for Asset Bundle
submissions (see next section), but for now there will be no public call
for content, because such calls typically cause much overhead and can
lead to too much variation in quality.

See the sections below for submission guidelines and distribution
suggestions.

## Potential asset contributors

Potential Blender Asset Bundle contributors:

- [Blenderkit](https://www.blenderkit.com/)
- . . .

## Asset guidelines

- **Accessible**

  
Bundled assets are an opportunity to help users learn. Users should be
able to “look into the box”, pick apart assets and open node groups to
learn about how they can create and manage such assets themselves.

Make sure assets are usable by novice users. For example, if materials
or Geometry Nodes objects exist of complex node structures, please group
nodes and give the inputs / outputs clear names.

- **Well-organized**

  
All assets should make good use of tags, descriptions and mention the
license.

E.g. materials could have a tag with the supported render engine name,
so users can filter by EEVEE or Cycles materials.

- **Encapsulated**

  
Dragging in an asset should not change scene settings, or add many
data-blocks to the scene. E.g. dragging in a studio light setup
shouldn’t adjust the render settings. So better make it a collection,
not a scene.

- **No usage restrictions, no advertisement**

  
We will only accept CC-0 licensed assets. These assets will be modified
by users and used for real work. Therefore, we cannot expect users to
credit the asset creators for these things.  
The bundle also shouldn't serve as advertisement platform for artists.
While authors will be credited using their full name, there should not
be links to their homepages, product mentions and the like.

- **Lightweight**

  
Not only should the bundle be free of charge and free to use under a
CC-0 license; it should also not bother users if they do not use the
bundle. E.g. the Blender download size shouldn’t include hundreds of
megabytes just for assets.

In order to keep the Asset Bundle lightweight, built-in assets will have
to make use of small, lightweight textures, and meshes will have to be
light.

- **Compatible**

  
Users will probably want to access the bundled asset library from
multiple Blender releases.

They need to be stored in a centralized location, with access from
multiple releases. The asset browser design already allows mounting
centralized asset libraries via the Preferences.

The assets should be compatible with coming Blender releases. Ideally
also with older Blender releases (at least starting with 2.80).

Additional compatibility checks could be added, so assets can only be
added to versions supporting the used feature(s).

Ideally assets would be render engine agnostic. Of course an EEVEE
specific material can be EEVEE only, but in general materials should
also work with Cycles, ideally also with other render engines.

- **Offline**

  
The Asset Bundle should be usable from Blender without internet
connection.

## How to provide the bundle?

The Asset Bundle is a directory of Blend files. Below are options how we
could provide it:

- **Bundle with Blender**

  
Include asset .blend files as part of the Blender download, like fonts
or scripts. These could also be compiled into the binary.

At some point (e.g. a background job after Blender startup) Blender
ensures the bundle is in a centralized location (e.g.
~/.config/blender/assets).

- ❗ Possible issues:
  * Violates zero cost paradigm.
  * Can easily bloat the Blender download size.
  * Assets may already be installed from another Blender version.

- **Generate**

  
Keep assets parametric, so they can be created 'on the fly' (e.g. by a
script). Blender can then dynamically generate the bundle in a
centralized default location as above.

- **Additional download from blender.org**

  
There could be helpers to make this easy and fast to do. E.g. a button
to open the download page in a web browser and a button to install the
downloaded library to a default location.

- **Online repository**

  
We could drop the requirement to be purely offline and just give the
user an option to display default assets from an online library.
Technically not supported yet, but might be an attractive, modern
solution we could support soon (in a sense, best of both worlds). Of
course the offline requirement has its merits.

- **Combination**

  
There could be combinations of the above. E.g. some bundled asset
.blends + some parametric ones created on the fly.

## How to install the bundle?

If the bundle is an additional download, how can it be installed easily
within Blender? Some options:

- Have the library as compressed archive with .blends. Let the user
  manage this and mount it in the Preferences.
- Add “Install…” button to locate and install archive. It will extract
  the archive to a default location (e.g. ~/.config/blender/assets),
  similar to adding an add-on.

## Should bundled assets be 'read only'?

- We might consider discouraging edits and put the library in a hidden
  directory. Otherwise updates to the bundle might be problematic.
- Versioning could become an issue too (e.g. user stores asset with beta
  Blender version, opens it with previous LTS).
