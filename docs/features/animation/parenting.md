# Animation & Rigging: Parenting

## Objects

Object parents can be set via the Object's properties panel, and via
operators. These mostly differ in the way they handle the Parent Inverse
matrix (recompute, reset, or don't touch) and the object's local
transform.

### Operators for Assigning Parents

- **Set Parent To Object**:
  - Set the parent,
  - recompute the parent inverse (see below),
  - keep local object transform as-is.
- **Set Parent To Object (Keep Transforms)**:
  - Copy the Object's current world transform to its local loc/rot/scale
    properties,
  - set the parent, and
  - recompute the parent inverse (see below).
- **Set Parent To Object (Without Inverse)**:
  - Set the parent,
  - reset the Parent Inverse matrix to the unit matrix,
  - reset the object location to zero.

### Properties Panel

Setting the parent via the Properties panel will reset the Parent
Inverse matrix to the unit matrix and leave the local transform as-is.

### Computing the Parent Inverse

Given object `O` and parent-to-be `P`.

- Create a temporary object with zero transform,
- set its parent to `P` (taking into account the parenting type),
- calculate its world transform,
- invert that transform, and
- store as `O.parent_inverse`.

## Bones

To be written.

## Vertex Parent

To be written.

## Special parents: Curve, Lattice, Armature

To be written.
