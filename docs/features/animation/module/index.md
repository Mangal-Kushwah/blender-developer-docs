# Animation & Rigging

The *Animation & Rigging module* covers the graph editor, dopespheet editor, NLA
editor, keyframes, drivers, constraints, armatures, and more.

Currently the module are working on **Project Baklava**:

- [2022: Announcement of Animation 2025 & initial workshop](https://code.blender.org/2022/11/the-future-of-character-animation-rigging/)
- [2023: Proposal for layered animation system](https://code.blender.org/2023/07/animation-workshop-june-2023/)
- [2024: Animation 2025 gets focused into Project Baklava](https://code.blender.org/2024/02/animation-2025-progress-planning/)


## Useful Links

- **[Module Chat][chat]:** the main communication hub of the module. Feel free to pop in and say hi!
- **[Video Calls][video]:** the one video call URL where all module discussions are held. This is used for the regular module meetings, as well as impromptu meetings.
- **[Meeting Agenda][agenda]:** calendar of the weekly module meetings.
- **[Tracker][tracker]:** for bug reports, pull requests, list of module members, etc.

[chat]: https://blender.chat/channel/animation-module
[video]: https://meet.waag.org/blender-animrig
[agenda]: https://stuvel.eu/anim-meetings/
[tracker]: https://projects.blender.org/blender/blender/wiki/Module:%20Animation%20&%20Rigging


## Subpages

- [Bigger Projects](bigger_projects.md): broad ideas for the future.
- [Weak Areas](weak_areas.md): areas   of the Animation & Rigging system that could use improvement.
- [Character Animation Workshop 2022](character_animation_workshop_2022.md)
