# Descriptor sets

> NOTE: Current implementation uses the 'old' descriptor set approach. The plan
> is to migrate to `KHR_EXT_descriptor_buffers` as descriptor sets aren't well
> suited for our use-case.

## Resource binding

Blender uses a resource binding approach that doesn't align with Vulkans binding approach.

* Blender uses a namespace per resource type; Uniform buffers, textures, images and Shader buffers are bound to their own list.
* Vulkan needs to bind these to shader locations.
* Textures/images and uniform buffers can be bound without a shader attached. Switching shaders should keep the bound resources.

### State manager

## What is a descriptor set?



### Descriptor Pools

