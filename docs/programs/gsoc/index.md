# Google Summer of Code

**Google Summer of Code (GSoC) is a Google program focused on bringing new
contributors into open source software development.**

![](../../images/GSoC-logo.png){width=120 align=left}

GSoC Contributors work with an open source organization on a 12+ week
programming project under the guidance of mentors.

Blender is happy to participate in this initiative since its beginning
in 2005!

<https://summerofcode.withgoogle.com>

## GSoC 2024

Blender Foundation has been accepted as a mentoring organization for GSoC 2024!

Applications for GSoC contributors open **March 18 -18:00 UTC** and close **April 2 - 18:00 UTC**.

## Projects from Previous years

[2023](2023.md) - [2022](2022.md) - [2021](2021.md) - [2020](2020.md) - [2019](2019.md) -
<span class="plainlinks">[2018](https://archive.blender.org/wiki/2015/index.php/Dev:Ref/GoogleSummerOfCode/2018/)</span> -
<span class="plainlinks">[2017](https://archive.blender.org/wiki/2015/index.php/Dev:Ref/GoogleSummerOfCode/2017/)</span> -
<span class="plainlinks">[2016](https://archive.blender.org/wiki/2015/index.php/Dev:Ref/GoogleSummerOfCode/2016/)</span> -
<span class="plainlinks">[2015](https://archive.blender.org/wiki/2015/index.php/Dev:Ref/GoogleSummerOfCode/2015/)</span> -
<span class="plainlinks">[2014](https://archive.blender.org/wiki/2015/index.php/Dev:Ref/GoogleSummerOfCode/2014/)</span> -
<span class="plainlinks">[2013](https://archive.blender.org/wiki/2015/index.php/Dev:Ref/GoogleSummerOfCode/2013/)</span> -
<span class="plainlinks">[2012](https://archive.blender.org/wiki/2015/index.php/Dev:Ref/GoogleSummerOfCode/2012/)</span> -
<span class="plainlinks">[2011](https://archive.blender.org/wiki/2015/index.php/Dev:Ref/GoogleSummerOfCode/2011/)</span> -
<span class="plainlinks">[2010](https://archive.blender.org/wiki/2015/index.php/Dev:Ref/GoogleSummerOfCode/2010/)</span> -
<span class="plainlinks">[2009](https://archive.blender.org/wiki/2015/index.php/Dev:Ref/GoogleSummerOfCode/2009/)</span> -
<span class="plainlinks">[2008](https://archive.blender.org/wiki/2015/index.php/Dev:Ref/GoogleSummerOfCode/2008/)</span> -
<span class="plainlinks">[2007](https://archive.blender.org/wiki/2015/index.php/Dev:Ref/GoogleSummerOfCode/2007/)</span> -
<span class="plainlinks">[2006](https://archive.blender.org/wiki/2015/index.php/Dev:Ref/GoogleSummerOfCode/2006/)</span> -
<span class="plainlinks">[2005](https://archive.blender.org/wiki/2015/index.php/Dev:Ref/GoogleSummerOfCode/2005/)</span>
