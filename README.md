# Blender Developer Documentation

New Blender Developer Documentation platform using [Material for
MkDocs](https://squidfunk.github.io/mkdocs-material/).<br/>
The generated documentation is live on
[developer.blender.org/docs](https://developer.blender.org/docs/)

This is the successor of the developer Wiki, which was
[archived](https://archive.blender.org/wiki/2024/) in January 2024.

## Building this Documentation Offline

The following assumes [Python 3](https://www.python.org/),
[pip](https://pypi.org/project/pip/), [Git](https://git-scm.com/) and
[Git LFS](https://git-lfs.com/) are installed.

**Set up Git LFS for the current user:**
```sh
git lfs install
```

**Clone the documentation sources:**
```sh
git clone https://projects.blender.org/blender/blender-developer-docs.git developer-docs
```
This will clone the sources into a `developer-docs` directory inside the current
one.

**Install all dependencies, such as
[Material for MkDocs](https://squidfunk.github.io/mkdocs-material/):**
```sh
python -m pip install -r requirements.txt
```

**Build this documentation with live reloading:**
```sh
mkdocs serve
```

Alternatively `mkdocs build` will generate the documentation as HTML into a
`site/` directory. Simply open `site/index.html` in a browser.

## Links

- **Chat**: [#developer-documentation on blender.chat](https://blender.chat/channel/developer-documentation).
